﻿using System.Collections;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ForTest
{
    public class Preparation : INotifyPropertyChanged
    {
        private int _id;
        private string _namePrep;
        private double _cost;
        private bool _availability;
        private string _quantity;
        private int _apteka_Id;
        CaseInsensitiveComparer comparer = new CaseInsensitiveComparer();
        public int Apteka_Id
        {
            get { return _apteka_Id; }
            set { _apteka_Id = value; OnPropertyChanged(nameof(Apteka_Id)); }
        }
        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged(nameof(Id)); }
        }
        public string NamePrep
        {
            get { return _namePrep; }
            set { _namePrep = value; OnPropertyChanged(nameof(NamePrep)); }
        }

        public double Cost
        {
            get { return _cost; }
            set { _cost = value; OnPropertyChanged(nameof(Cost)); }
        }

        public bool Availability
        {
            get { return _availability; }
            set { _availability = value; OnPropertyChanged(nameof(Availability)); }
        }

        public string Quantity
        {
            get { return _quantity; }
            set { _quantity = value; OnPropertyChanged(nameof(Quantity)); }
        }
       // Сортировки
        public int CompareToLow(Preparation cost1, Preparation cost2)
        {
            int result = comparer.Compare(cost1.Cost, cost2.Cost);
            return result;
        }
        public int ComparerToHigh(Preparation cost1, Preparation cost2)
        {
            int result = comparer.Compare(cost2.Cost, cost1.Cost);
            return result;
        }
        public int ComparerToName(Preparation name1, Preparation name2)
        {
            int result = comparer.Compare(name1.NamePrep, name2.NamePrep);
            return result;
        }
        public int ComparerToAvailability(Preparation ava1, Preparation ava2)
        {
            int result = comparer.Compare(ava2.Availability, ava1.Availability);
            return result;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
