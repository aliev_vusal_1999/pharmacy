﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Input;
using System.Data.SQLite;
using System.Windows;

namespace ForTest
{
    
    public class RelayCommand : ICommand
    {
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            _execute.Invoke();
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            if (parameter is T arg)
            {
                _execute.Invoke(arg);
            }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter is T arg)
            {
                return _canExecute?.Invoke(arg) ?? true;
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
   
    public class MainViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Apteka> Aptekas { get; set; }
        private SQLiteConnection dbConnection;

        // Соединение с базой данных
        public void ConnectionToDb()
        {
            dbConnection = new SQLiteConnection("Data Source=D:\\flashka8gb\\С#\\AptekaNewDB\\AptekaAppNewDB\\ForTest\\newdb.db; Version=3");
        }
        // Считывание из базы данных.
        public void LoadDb()
        {            
            SQLiteCommand load = dbConnection.CreateCommand();
            load.CommandText = "select * from AptekaTable";
            SQLiteDataReader sql = load.ExecuteReader();

            while (sql.Read())
            {
                Apteka apteka = new Apteka();
                apteka.Id = Convert.ToInt32(sql["Id"]);
                apteka.Name = Convert.ToString(sql["Name"]);
                apteka.Address = Convert.ToString(sql["Address"]);
                apteka.TimeWorking = Convert.ToString(sql["TimeWorking"]);
                Aptekas.Add(apteka);
            }
        }
        public MainViewModel()
        {
            Aptekas = new ObservableCollection<Apteka>();
            ConnectionToDb();            
            dbConnection.Open();
            LoadDb();
        }
        private string _name;
        private string _address;
        private string _timeWorking;
        private Apteka _selectedApteka;
        

        private ICommand _addApteka;
        private ICommand _delApteka;
        private ICommand _searchApteka;
        private ICommand _showPreparation;
        private ICommand _sortNameApteka;
        private ICommand _sortAddressApteka;
        

        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(nameof(Name)); }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; OnPropertyChanged(nameof(Address)); }
        }
        public string TimeWorking
        {
            get { return _timeWorking; }
            set { _timeWorking = value; OnPropertyChanged(nameof(TimeWorking)); }
        }
        public Apteka SelectedApteka
        {
            get { return _selectedApteka; }
            set { _selectedApteka = value; OnPropertyChanged(nameof(SelectedApteka)); }
        }
        // Команда добавления
        public ICommand AddApteka
        {
            get { return _addApteka ?? (_addApteka = new RelayCommand(AddAptekaExecute, AddAptekaCanExecute)); }
        }
        private void AddAptekaExecute()
        {
            Apteka apteka = new Apteka();
            apteka.Address = Address;
            apteka.Name = Name;
            apteka.TimeWorking = TimeWorking;
            Aptekas.Add(apteka);

            SQLiteCommand add = dbConnection.CreateCommand();
            add.CommandText = "insert into AptekaTable(Name,Address,TimeWorking) values(@name,@address,@time)";
            add.Parameters.Add("@name", DbType.String).Value = apteka.Name;
            add.Parameters.Add("@address", DbType.String).Value = apteka.Address;
            add.Parameters.Add("@time", DbType.String).Value = apteka.TimeWorking;
            add.ExecuteNonQuery();

            Address = "";
            Name = "";
            TimeWorking = "";
        }
        private bool AddAptekaCanExecute()
        {
            if ( (Name != "" && Name != null) && (Address != "" && Address != null) && (TimeWorking != "" && TimeWorking != null))
                return true;
            else
                return false;
        }
        //Команда удаления
        public ICommand DelApteka
        {
            get { return _delApteka ?? (_delApteka = new RelayCommand<Apteka>(DelAptekaExecute, DelAptekaCanExecute)); }
        }
        private void DelAptekaExecute(Apteka apteka)
        {
            SQLiteCommand del = dbConnection.CreateCommand();
            del.CommandText = "delete from AptekaTable where Name like @name and Address like @address ";
            del.Parameters.Add("@name", DbType.String).Value = apteka.Name;
            del.Parameters.Add("@address", DbType.String).Value = apteka.Address;
            del.ExecuteNonQuery();

            Aptekas.Remove(apteka);
        }
        private bool DelAptekaCanExecute(Apteka apteka)
        {
            if ( Aptekas.Count > 0)
                return true;
            else
                return false;
        }
        // Поиск аптеки       
        public ICommand SearchApteka
        {
            get { return _searchApteka ?? (_searchApteka = new RelayCommand<string>(SearchAptekaExecute, SearchAptekaCanExecute)); }
        }
        private void SearchAptekaExecute(string str)
        {
            bool searchResult = false;
            foreach (var item in Aptekas)
            {
                if (item.Name == str || item.Address == str || item.TimeWorking == str)
                {
                    SelectedApteka = item;
                    searchResult = true;
                    break;
                }
            }
            if (searchResult == false)
                MessageBox.Show("Аптека не найдена!");
        }
        private bool SearchAptekaCanExecute(string str)
        {
            if (str != "")
                return true;
            else
                return false;
        }
        // Сортировка по названию
        public ICommand SortNameApteka
        {
            get { return _sortNameApteka ?? (_sortNameApteka = new RelayCommand(SortNameAptekaExecute, SortNameAptekaCanExecute)); }
        }
        private void SortNameAptekaExecute()
        {
            Apteka apteka = new Apteka();
            List<Apteka> tempAptekas = Aptekas.ToList<Apteka>();
            tempAptekas.Sort(apteka.ComparerToNameApteka);
            Aptekas.Clear();
            foreach (var item in tempAptekas)
            {
                Aptekas.Add(item);
            }
        }
        private bool SortNameAptekaCanExecute()
        {
            if (Aptekas.Count > 0)
                return true;
            else
                return false;
        }
        // Сортировка по адресу
        public ICommand SortAddressApteka
        {
            get { return _sortAddressApteka ?? (_sortAddressApteka = new RelayCommand(SortAddressAptekaExecute, SortAddressAptekaCanExecute)); }
        }
        private void SortAddressAptekaExecute()
        {
            Apteka apteka = new Apteka();
            List<Apteka> tempAptekas = Aptekas.ToList<Apteka>();
            tempAptekas.Sort(apteka.ComparerToAddressApteka);
            Aptekas.Clear();
            foreach (var item in tempAptekas)
            {
                Aptekas.Add(item);
            }
        }
        private bool SortAddressAptekaCanExecute()
        {
            if (Aptekas.Count > 0)
                return true;
            else
                return false;
        }
        // Новое окно (препараты)       
        public ICommand ShowPreparation
        {
            get { return _showPreparation ?? (_showPreparation = new RelayCommand<Apteka>(ShowPreparationExecute, ShowPreparationaCanExecute)); }
        }
        private void ShowPreparationExecute(Apteka apteka)
        {
            apteka.LoadDb(apteka.Id);
            PreparationWindow preparationWindow = new PreparationWindow();            
            preparationWindow.DataContext = apteka;            
            preparationWindow.Show();
        }
        private bool ShowPreparationaCanExecute(Apteka apteka)
        {
            return true;
        }        

        public string Error => throw new NotImplementedException();
        public string this[string columnName] => throw new NotImplementedException();
        private void OnPropertyChanged(string PropertyName)
        {
            if (PropertyName == null)
                return;
            PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
