﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using System.Data.SQLite;
using System.Data;
using System.Collections;
using System.Windows;

namespace ForTest
{   
    public class Apteka : INotifyPropertyChanged
    {
        private SQLiteConnection dbConnection;
        
        // Соединение с базой данных
        public void ConnectionToDb()
        {
            dbConnection = new SQLiteConnection("Data Source=D:\\flashka8gb\\С#\\AptekaNewDB\\AptekaAppNewDB\\ForTest\\newdb.db; Version=3");
        }
        // Считывание из базы данных.
        public void LoadDb(int apteka_id)
        {
            Preparations.Clear();
            SQLiteCommand load = dbConnection.CreateCommand();
            load.CommandText = "select * from PreparationTable";
            SQLiteDataReader sql = load.ExecuteReader();
            id_save = apteka_id;
            while (sql.Read())
            {
                Preparation preparation = new Preparation();
                preparation.Id = Convert.ToInt32(sql["Id"]);
                preparation.Apteka_Id = Convert.ToInt32(sql["Apteka_Id"]);
                preparation.NamePrep = Convert.ToString(sql["NamePrep"]);
                preparation.Cost = Convert.ToDouble(sql["Cost"]);
                preparation.Availability = Convert.ToBoolean(sql["Availability"]);
                preparation.Quantity = Convert.ToString(sql["Quantity"]);
                if (apteka_id == preparation.Apteka_Id)
                {
                    id_save = apteka_id;
                    Preparations.Add(preparation);
                }
            }
        }
        CaseInsensitiveComparer comparer = new CaseInsensitiveComparer();
        public Apteka()
        {             
            Preparations = new ObservableCollection<Preparation>();
            ConnectionToDb();
            dbConnection.Open();
        }
        public int id_save;

        private int _id;
        private string _name;
        private string _address;
        private string _timeWorking;
        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged(nameof(Id)); }
        }
        public string Name 
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(nameof(Name)); }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; OnPropertyChanged(nameof(Address)); }
        }
        public string TimeWorking
        {
            get { return _timeWorking; }
            set { _timeWorking = value; OnPropertyChanged(nameof(TimeWorking)); }
        }

        public ObservableCollection<Preparation> Preparations { get; set; }
       
        private double _cost;
        private string _namePrep;
        private string _quantity;
        private int _apteka_Id;
        private Preparation _selectedPreparation;

        private ICommand _addPreparation;
        private ICommand _delPreparation;
        private ICommand _searchPreparation;
        private ICommand _checkPreparation;
        private ICommand _costSortLow;
        private ICommand _costSortHigh;
        private ICommand _sortName;
        private ICommand _sortAvailability;

        public int Apteka_Id
        {
            get { return _apteka_Id; }
            set { _apteka_Id = value; OnPropertyChanged(nameof(Apteka_Id)); }
        }
        public string NamePrep
        {
            get { return _namePrep; }
            set { _namePrep = value; OnPropertyChanged(nameof(NamePrep)); }
        }        
        public double Cost
        {
            get { return _cost; }
            set
            {
                _cost = value;
                OnPropertyChanged(nameof(Cost));
            }
        }
        public string Quantity
        {
            get { return _quantity; }
            set { _quantity = value; OnPropertyChanged(nameof(Quantity)); }
        }
        public Preparation SelectedPreparation
        {
            get { return _selectedPreparation; }
            set { _selectedPreparation = value; OnPropertyChanged(nameof(SelectedPreparation)); }
        }
        // Добавление препаратов       
        public ICommand AddPreparation
        {
            get { return _addPreparation ?? (_addPreparation = new RelayCommand(AddPreparationExecute, AddPreparationCanExecute)); }
        }
        private void AddPreparationExecute()
        {
            Preparation preparation = new Preparation();
            preparation.NamePrep = NamePrep;
            preparation.Cost = Cost;
            preparation.Availability = false;
            preparation.Quantity = Quantity;
            preparation.Apteka_Id = id_save;
            Preparations.Add(preparation);

            SQLiteCommand add = dbConnection.CreateCommand();
            add.CommandText = "insert into PreparationTable(NamePrep,Cost,Availability,Quantity,Apteka_Id)" +
                " values(@name,@cost,@ava,@quantity,@ap_id)";
            add.Parameters.Add("@name", DbType.String).Value = preparation.NamePrep;
            add.Parameters.Add("@cost", DbType.Double).Value = preparation.Cost;
            add.Parameters.Add("@ava", DbType.Boolean).Value = preparation.Availability;
            add.Parameters.Add("@quantity", DbType.String).Value = preparation.Quantity;
            add.Parameters.Add("@ap_id", DbType.Int32).Value = id_save;
            add.ExecuteNonQuery();

            NamePrep = "";
            Cost = 0;
            Quantity = "";
        }
        private bool AddPreparationCanExecute()
        {
            if ((NamePrep != "" && NamePrep != null)
                && (Quantity != "" && Quantity != null) && Cost != 0)
                return true;
            else
                return false;
        }
        // Удаление препаратов
        public ICommand DelPreparation
        {
            get { return _delPreparation ?? (_delPreparation = new RelayCommand<Preparation>(DelPreparationExecute, DelPreparationCanExecute)); }
        }
        private void DelPreparationExecute(Preparation preparation)
        {
            SQLiteCommand del = dbConnection.CreateCommand();
            del.CommandText = "delete from PreparationTable where NamePrep like @name";
            del.Parameters.Add("@name", DbType.String).Value = preparation.NamePrep;
            del.ExecuteNonQuery();

            Preparations.Remove(preparation);
        }
        private bool DelPreparationCanExecute(Preparation preparation)
        {
            if (Preparations.Count > 0)
                return true;
            else
                return false;
        }
        // Поиск препаратов       
        public ICommand SearchPreparation
        {
            get { return _searchPreparation ?? (_searchPreparation = new RelayCommand<string>(SearchPreparationExecute, SearchPreparationCanExecute)); }
        }
        private void SearchPreparationExecute(string str)
        {
            bool searchResult = false; 
            foreach (var item in Preparations)
            {
                if (item.NamePrep == str || Convert.ToString(item.Cost) == str)
                {
                    SelectedPreparation = item;
                    searchResult = true;
                    break;
                }
            }
            if (searchResult == false)
                MessageBox.Show("Препарат не найден!");
        }
        private bool SearchPreparationCanExecute(string str)
        {
            if (str != "")
                return true;
            else
                return false;
        }
        // Обновить данные препаратов в базе данных
        public ICommand CheckPreparation
        {
            get { return _checkPreparation ?? (_checkPreparation = new RelayCommand<Preparation>(CheckPreparationExecute, CheckPreparationCanExecute)); }
        }
        private void CheckPreparationExecute(Preparation preparation)
        {
            SQLiteCommand check = dbConnection.CreateCommand();
            check.CommandText = "update PreparationTable set Availability = @ava where Id like @id";
            check.Parameters.Add("@ava", DbType.Boolean).Value = preparation.Availability;
            check.Parameters.Add("@id", DbType.Int32).Value = preparation.Id;
            check.ExecuteNonQuery();
        }
        private bool CheckPreparationCanExecute(Preparation preparation)
        {
            return true;
        }
        // Сортировка по возрастанию
        public ICommand CostSortLow
        {
            get { return _costSortLow ?? (_costSortLow = new RelayCommand(CostSortLowExecute, CostSortLowCanExecute)); }
        }
        private void CostSortLowExecute()
        {
            Preparation preparation = new Preparation();
            List<Preparation> tempPreparations = Preparations.ToList<Preparation>();
            tempPreparations.Sort(preparation.CompareToLow);
            Preparations.Clear();
            foreach(var item in tempPreparations)
            {
                Preparations.Add(item);
            }
        }
        private bool CostSortLowCanExecute()
        {
            if (Preparations.Count > 0)
                return true;
            else
                return false;
        }
        // Сортировка по убыванию
        public ICommand CostSortHigh
        {
            get { return _costSortHigh ?? (_costSortHigh = new RelayCommand(CostSortHighExecute, CostSortHighCanExecute)); }
        }
        private void CostSortHighExecute()
        {
            Preparation preparation = new Preparation();
            List<Preparation> tempPreparations = Preparations.ToList<Preparation>();
            tempPreparations.Sort(preparation.ComparerToHigh);
            Preparations.Clear();
            foreach (var item in tempPreparations)
            {
                Preparations.Add(item);
            }
        }
        private bool CostSortHighCanExecute()
        {
            if (Preparations.Count > 0)
                return true;
            else
                return false;
        }
        // Сортировка по названию
        public ICommand SortName
        {
            get { return _sortName ?? (_sortName = new RelayCommand(SortNameExecute, SortNameCanExecute)); }
        }
        private void SortNameExecute()
        {
            Preparation preparation = new Preparation();
            List<Preparation> tempPreparations = Preparations.ToList<Preparation>();
            tempPreparations.Sort(preparation.ComparerToName);
            Preparations.Clear();
            foreach (var item in tempPreparations)
            {
                Preparations.Add(item);
            }
        }
        private bool SortNameCanExecute()
        {
            if (Preparations.Count > 0)
                return true;
            else
                return false;
        }
        // Сортировка по наличию
        public ICommand SortAvailability
        {
            get { return _sortAvailability ?? (_sortAvailability = new RelayCommand(SortAvailabilityExecute, SortAvailabilityCanExecute)); }
        }
        private void SortAvailabilityExecute()
        {
            Preparation preparation = new Preparation();
            List<Preparation> tempPreparations = Preparations.ToList<Preparation>();
            tempPreparations.Sort(preparation.ComparerToAvailability);
            Preparations.Clear();
            foreach (var item in tempPreparations)
            {
                Preparations.Add(item);
            }
        }
        private bool SortAvailabilityCanExecute()
        {
            if (Preparations.Count > 0)
                return true;
            else
                return false;
        }
        // Сортировки Аптек        
        public int ComparerToNameApteka(Apteka name1, Apteka name2)
        {
            int result = comparer.Compare(name1.Name, name2.Name);
            return result;
        }
        public int ComparerToAddressApteka(Apteka address1, Apteka address2)
        {
            int result = comparer.Compare(address1.Address, address2.Address);
            return result;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }       
    }
}
